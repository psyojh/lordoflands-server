import threading
import time
from .models import *
import datetime
from .views import *
from django.utils import timezone
from .Map_Progress import *
from random import randint
from .Map_Progress import *


exitFlag = 0
game = True
class TimeCheckingThread (threading.Thread):
    def __init__(self, threadID, name, counter,session):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter
        self.session = session

    def run(self):
        print ("Starting " + self.name)
        time = timezone.now()
        current_session = Session.objects.get(Session_id = self.session)
        current_session.start_time = time + datetime.timedelta(0,5)
        current_session.save()
        TimeCheck(self.name, self.counter, 5)
        current_session = Session.objects.get(Session_id = self.session)
        current_session.Is_playing = True
        current_session.save()
        BuildingThread = BuildingEconomyThread(2,'BuildThread',10,self.session)
        BuildingThread.start()
        UnitThread = UnitMovementThread(3,'UnitThread',5,self.session)
        UnitThread.start()
def TimeCheck(threadName, delay, counter):
    while counter:
        if exitFlag:
            threadName.exit()
        time.sleep(delay)
        counter -= 1

class PlayerElimination(threading.Thread):
    def __init__(self,threadID,name,counter,session):
        self.threadID = threadID
        self.name = name
        self.counter = counter
        self.session = session
    def run(self):
        print("starting " + self.name)
        playerEliminate()
        print("Exiting " + self.name)


def playerEliminate(session):
    global game

    while game == True:
        Session_Players = Session_Players.objects.filter(Session_id = session)
        for session in Session_Players:
            if(Buildings.objects.filter(Building_Owner = session.Session_Players_id,Building_Type = 3 ).count() == 0):
                elimation = Session_players.objects.filter(Session_Players_id = session.Session_Players_id)
                elimination.eliminated = True
            if session.eliminated == True:
                count += 1
        if count == 1:
            game = False
            winning_player = Session_Players.objects.get(eliminated = False)
            print("Game Winner " + winning_player.User_Acc_id)





class BuildingEconomyThread(threading.Thread):
    def __init__(self,threadID,name, counter, session):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter
        self.session = session
    def run(self):
        print ("Starting " + self.name)
        BuildingCheck(self.name,self.counter,self.session)
        print("Exiting " + self.name)

def BuildingCheck(threadName, delay,session):
    print("STarter")
    global game
    while game == True:
        if exitFlag:
            threadName.exit()
        current_session = Session.objects.get(Session_id = session)
        if current_session.Is_finished:
            ending = True
        else:
            Mapping = Map.objects.get(Session_id = session)
            map_size = Mapping.Map_row
            current_resources = Mapping.ResourceMap
            current_territory = Mapping.TerritoryMap
            Current_resource_mat = MapToMatrix(current_resources,map_size)
            current_territory_mat = MapToMatrix(current_territory,map_size)

            for x in range(0,map_size):
                for y in range(0,map_size):
                    if Current_resource_mat[x][y] == '3':
                        if int(current_territory_mat[x][y]) > 10:
                            player_id = float(current_territory_mat[x][y])
                            Session_player = Session_Players.objects.get(Session_Players_id = player_id)
                            total_gold = int(Session_player.Gold) + 5
                            Session_player.Gold = total_gold
                            Session_player.save()
                    elif Current_resource_mat[x][y] == '4':
                        if int(current_territory_mat[x][y])  > 10:
                            player_id = float(current_territory_mat[x][y])
                            Session_player = Session_Players.objects.get(Session_Players_id = player_id)
                            total_food = int(Session_player.Food) + 5
                            print (total_food)
                            Session_player.Food = total_food
                            Session_player.save()
                    elif Current_resource_mat[x][y] == '5':
                        if int(current_territory_mat[x][y]) > 10:
                            player_id = float(current_territory_mat[x][y])
                            Session_player = Session_Players.objects.get(Session_Players_id = player_id)
                            total_gold = int(Session_player.Gold) + 8
                            Session_player.Gold  = total_gold
                            Session_player.save()
                    elif Current_resource_mat[x][y] == '6':
                        if int(current_territory_mat[x][y])  > 10:
                            player_id = float(current_territory_mat[x][y])
                            Session_player = Session_Players.objects.get(Session_Players_id = player_id)
                            total_build = int(Session_player.Building_resources) + 8
                            Session_player.Building_resources = total_build
                            Session_player.save()
        time.sleep(delay)


class UnitMovementThread(threading.Thread):
    def __init__(self,threadID,name, counter, session):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.session = session

    def run(self):
        print ("Starting " + self.name)
        UnitCheck(self.name,self.session)
        print("Exiting " + self.name)

def UnitCheck(threadName,session):
    global game
    while game == True:

        Unit_move = Unit_Movement.objects.filter(Session_id = session)

        for i in Unit_move:
            if (i.end_time - timezone.now()).total_seconds() < 5 or (timezone.now() - i.end_time).total_seconds() > - 5:
                Unit_Moving(i.unit_id_id,session,i.Unit_X,i.Unit_Y)
            else:
                continue

        time.sleep(5)



def Unit_Moving(unit,session,Unit_X,Unit_Y):
    Mapping = Map.objects.get(Session_id = session)
    UnitMatrix = MapToMatrix(Mapping.UnitMap,Mapping.Map_col)
    Unit = Units.objects.get(Units_id = int(unit))
    TerritoryMatrix = MapToMatrix(Mapping.TerritoryMap,Mapping.Map_col)
    BuildingMatrix = MapToMatrix(Mapping.ResourceMap,Mapping.Map_col)
    map_size = Mapping.Map_col
    for x in range(0,map_size):
        for y in range(0,map_size):
            if UnitMatrix[y][x] == str(unit):
                if int(BuildingMatrix[Unit_Y][Unit_X]) > 1 and TerritoryMatrix[Unit_Y][Unit_X] != str(unit):
                    defendBuilding = Buildings.objects.get(Building_X = Unit_Y,Building_Y = Unit_X,Session_Id = session)
                    winner = BuildingAttack(Unit,defendBuilding)
                    if 'B' in winner:
                        print('UNIT WIN')
                        BuildingTakeover(Unit_X,Unit_Y,Unit,defendBuilding,session)
                        #Unit_Move_Build_Check(x,y,Unit_X,Unit_Y,UnitMatrix,TerritoryMatrix,BuildingMatrix,Unit,session)
                    elif 'U' in winner:
                        print('Building WIN')
                        UnitMatrix[y][x] = '0'
                        UnitMatrix[Unit_Y][Unit_X] = '0'

                        Unit.delete()
                        TerritoryMatrix[y][x] = str(defendBuilding.Building_Owner_id)

                        territory = listsToMap(TerritoryMatrix,map_size)
                        units = listsToMap(UnitMatrix,map_size)

                        Mapping.UnitMap = units
                        Mapping.TerritoryMap = territory
                        Mapping.save()

                elif UnitMatrix[Unit_Y][Unit_X] != str(unit) and UnitMatrix[Unit_Y][Unit_X] != '0':
                    opUnit = Units.objects.get(Units_id = int(UnitMatrix[Unit_Y][Unit_X]))
                    winner = int(UnitAttack(Unit,opUnit))
                    print("WINNER")
                    print(winner)
                    if winner == unit:
                        Unit_Move_Build_Check(x,y,Unit_X,Unit_Y,UnitMatrix,TerritoryMatrix,BuildingMatrix,Unit,session)
                    elif winner == opUnit.Units_id:
                        UnitLoss(Unit,opUnit,x,y,UnitMatrix,TerritoryMatrix,session)
                else:
                    Unit_Move_Build_Check(x,y,Unit_X,Unit_Y,UnitMatrix,TerritoryMatrix,BuildingMatrix,Unit,session)

def Unit_Move_Build_Check(prev_X,prev_Y,next_X,next_Y, unit_Map, territory_Map,building_map,unit,session):
    Mapping = Map.objects.get(Session_id = session)
    map_size = Mapping.Map_col

    count = 0

    if prev_Y % 2 == 1:
        for m in range (prev_Y - 1, prev_Y + 1):
            for n in range(prev_X-1 , prev_X + 2):
                if int(building_map[m][n]) >  1:
                    count +=1
                else:
                    continue
        if count == 0:
            unit_Map[prev_Y][prev_X] = '0'
            territory_Map[prev_Y][prev_X] = '0'
            unit_Map[next_Y][next_X] = str(unit.Units_id)
            territory_Map[next_Y][next_X] = str(unit.Unit_Owner_id)
        else:
            count = 0
            unit_Map[prev_Y][prev_X] = '0'
            unit_Map[next_Y][next_X] = str(unit.Units_id)
            territory_Map[prev_Y][prev_X] = str(unit.Unit_Owner_id)
            territory_Map[next_Y][next_X] = str(unit.Unit_Owner_id)
    else:
        for m in range (prev_Y , prev_Y + 2):
            for n in range(prev_X -1, prev_X + 2):
                if int(building_map[m][n]) > 1 :
                    count +=1
                else:
                    continue
        if count == 0 :
            count = 0
            unit_Map[prev_Y][prev_X] = '0'
            territory_Map[prev_Y][prev_X] = str(unit.Unit_Owner_id)

            #territory_Map[prev_Y][prev_X] = '0'

            unit_Map[next_Y][next_X] =  str(unit.Units_id)
            territory_Map[next_Y][next_X] = str(unit.Unit_Owner_id)
        else:
            count = 0
            unit_Map[prev_Y][prev_X] = '0'
            unit_Map[next_Y][next_X] =  str(unit.Units_id)
            territory_Map[prev_Y][prev_X] = str(unit.Unit_Owner_id)
            territory_Map[next_Y+1][next_X] = str(unit.Unit_Owner_id)
            territory_Map[next_Y][next_X] = str(unit.Unit_Owner_id)

    print("UPDTED")
    territory = listsToMap(territory_Map,map_size)
    units = listsToMap(unit_Map,map_size)
    Mapping.UnitMap = units
    Mapping.TerritoryMap = territory
    Mapping.save()



def Territory_moving(owner,territory_map,previous_pos,current_pos):
    if i == unit:
        pos = unit_map.index(i)
        print (pos)
        unit_map[pos] = 1
        territory_map[pos]

def UnitAttack(hostUnit,opUnit):
    #host = Units.objects.get(Units_id = hostUnit)
    #opponent = Units.objects.get(Units_id = opUnit)
    session = Session.objects.get(Session_id = opUnit.Session_Id_id)
    if Unit_Attacks.objects.filter(host_unit_id = hostUnit, opp_unit_id = opUnit).count() == 1:
        return Unit_Attacks.objects.get(host_unit_id = hostUnit, opp_unit_id = opUnit).result_of_battle
    else:
        host_size = int(hostUnit.Unit_Size) / 100
        op_size = int(opUnit.Unit_Size) / 100
        host_attack = int(hostUnit.Unit_Attack) * host_size
        op_attack = int(opUnit.Unit_Attack) * op_size

        if host_attack < op_attack:
            winner = opUnit.Units_id
        elif host_attack > op_attack:
            winner = hostUnit.Units_id
        else:
            if randint(0,9) < 5:
                winner = hostUnit.Units_id
            else:
                winner = opUnit.Units_id

        battle = Unit_Attacks(host_unit_id = hostUnit,opp_unit_id = opUnit,Session_id = session,result_of_battle = str(winner))
        battle.save()
        return winner
def BuildingAttack(hostUnit,building):
    session = Session.objects.get(Session_id = hostUnit.Session_Id_id)
    if Building_Attacks.objects.filter(unit = hostUnit, building = building).count() == 1:
        return Building_Attacks.objects.get(unit = hostUnit, building = building).result_of_battle
    else:
        unitSize = int(hostUnit.Unit_Size)/100
        buildingSize = int(building.Building_defense)/100
        unitAttack = int(hostUnit.Unit_Attack) * unitSize
        buildingAttack = int(building.Building_Attack) * buildingSize

        if unitAttack < buildingAttack:
            winner = 'B '+str(building.Building_id)
            endsize = int(building.Building_defense - unitAttack * 10)
            building.Building_defense = endsize
            building.Building_defense = endsize
            building.save()
        elif unitAttack > buildingAttack:
            winner = 'U '+str(hostUnit.Units_id)
            endsize = int(hostUnit.Unit_Size - Building_Attack * 10)
            hostUnit.Unit_Size = endsize
            hostUnit.save()
        else:
            winner = 'B '+str(building.Building_id)
            endsize = int(building.Building_defense / 10)
            building.Building_defense = endsize
            print(endsize)
            print('okokokkokokkk')
            building.save()

        battle = Building_Attacks(unit = hostUnit,building=building,Session_id=session,result_of_battle= winner)
        battle.save()
        return winner

def UnitLoss(hostUnit,opUnit,x,y, UnitMatrix, TerritoryMatrix,session):
    Mapping = Map.objects.get(Session_id = session)
    map_size = Mapping.Map_col

    hostUnit.delete()

    UnitMatrix[y][x] = str(opUnit.Units_id)
    TerritoryMatrix[y][x] = str(opUnit.Unit_Owner_id)

    territory = listsToMap(TerritoryMatrix,map_size)
    units = listsToMap(UnitMatrix,map_size)

    Mapping.UnitMap = units
    Mapping.TerritoryMap = territory
    Mapping.save()

def BuildingTakeover(x,y,Unit,Building,session):
    Mapping = Map.objects.get(Session_id = session)
    territory_map = MapToMatrix(Mapping.TerritoryMap,Mapping.Map_col)

    Building.Building_Owner = Unit.Unit_Owner

    if y % 2 == 1:
        for m in range (x - 1, x + 1):
            for n in range(y-1 , y + 2):
                territory_map[n][m] = str(Unit.Unit_Owner_id)
                territory_map[y][x+1] = str(Unit.Unit_Owner_id)
    else:
        for m in range (x , x + 2):
            for n in range(y -1, y + 2):
                territory_map[n][m] = str(Unit.Unit_Owner_id)
                territory_map[y][x-1] = str(Unit.Unit_Owner_id)


    territory = listsToMap(territory_map,Mapping.Map_col)
    Building.save()
    Mapping.TerritoryMap = territory
    Mapping.save()

def MatrixToMap(Matrix,col_num,row_num):
	mapping = []
	for y in range(col_num):
		for x in range(row_num):
			mapping.append(Matrix[x][y])
		mapping.append('r')
	return mapping

def listsToMap(lists,MapSize):
	mapping = []
	for y in range(0,MapSize):
		for x in range(0,MapSize):
			lists[x][y] = lists[x][y].replace("\'","")
			if lists[x][y] == ' \'0\']':
				mapping.append('0')
			if lists[x][y] == ' [\'0\'':
				mapping.append('0')
			else:
				mapping.append(lists[y][x])
		mapping.append('r')

	return mapping

def MapToMatrix(Map,MapSize):
	list_of_rows = []
	split_list = []
	row = []
	i = "  "
	Map = Map.replace("[", "")
	Map = Map.replace("]", "")
	Map = Map.replace(" ", "")
	Map = Map.replace("\'","")

	split_list = Map.split(',')

	for i in split_list:
		if i == "r":
			list_of_rows.append(list(row))
			row = []
		elif i == "," or i == " "or i== "[" or i == "]" or i == "\""or i == "\'" :
			continue
		else:
			row.append(i)
	return list_of_rows
