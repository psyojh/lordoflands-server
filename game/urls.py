from django.conf.urls import url
from . import views
from rest_framework_swagger.views import get_swagger_view
from rest_framework import routers
from views import User_Account_ViewSet

schema_view = get_swagger_view(title='Pastebin API')



user_list = User_Account_ViewSet.as_view({
    'get': 'list'
})

urlpatterns = [
	url(r'^user_account/$', user_list,name='user-list'),
    url(r'^swagger/$', schema_view)

	'''url(r'^session/$', views.SessionList),
	url(r'^game_stats/$',views.GameStats),
	url(r'^session-player/$',views.SessionPlayers),
    '''

]
