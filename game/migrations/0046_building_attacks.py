# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-06 07:36
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0045_auto_20170404_1029'),
    ]

    operations = [
        migrations.CreateModel(
            name='Building_Attacks',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time_of_battle', models.DateTimeField(auto_now_add=True)),
                ('result_of_battle', models.CharField(default='Pending', max_length=100)),
                ('Session_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='game.Session')),
                ('building', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='game.Buildings')),
                ('unit', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='game.Units')),
            ],
        ),
    ]
