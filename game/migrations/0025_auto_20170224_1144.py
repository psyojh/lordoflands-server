# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-24 11:44
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0024_auto_20170224_0907'),
    ]

    operations = [
        migrations.RenameField(
            model_name='units',
            old_name='Unit_name',
            new_name='Unit_Name',
        ),
    ]
