# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-09 14:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0040_session_is_finished'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='unit_movement',
            name='current_movement_tile',
        ),
        migrations.AddField(
            model_name='unit_movement',
            name='Unit_X',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='unit_movement',
            name='Unit_Y',
            field=models.IntegerField(default=0),
        ),
    ]
