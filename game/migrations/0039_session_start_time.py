# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-07 15:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0038_auto_20170306_1521'),
    ]

    operations = [
        migrations.AddField(
            model_name='session',
            name='start_time',
            field=models.DateTimeField(null=True),
        ),
    ]
