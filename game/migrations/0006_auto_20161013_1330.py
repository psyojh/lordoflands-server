# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-10-13 13:30
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0005_auto_20161012_1244'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user_account',
            old_name='Full_name',
            new_name='full_name',
        ),
    ]
