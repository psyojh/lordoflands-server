import random
import math

setPos = False
def Townhall_gen(Terrain,ResourceMap,col_num,row_num,num_players):
    Coastline(col_num,row_num,row_num*0.9,0,ResourceMap)
    Coastline(col_num/10,row_num,0,0,ResourceMap)
    Coastline(col_num,row_num/10,0,0,ResourceMap)
    Coastline(col_num,row_num,0,col_num*0.9,ResourceMap)

    if num_players == 3:
        map_quadrants = 3
    elif 4 <= num_players <= 6:
        map_quadrants = 9
    elif 7 <= num_players <= 10:
        map_quadrants = 25

    space = quadrant(map_quadrants,row_num,col_num)

    if num_players == 3:
        quadrants = [False,False,False,False]
        global setPos
        setPos = True
        for i in range(0,3):
            setPos = True
            while setPos:
                TerrainCount = 0
                quad = random.randint(0,4)
                if quad == 1 and quadrants[0]== False:
                    #set quad one
                    ResourceMap = quadrantPlacement(2,2,10,10,0,Terrain,quadrants,ResourceMap)
                elif quad == 2 and quadrants[1] == False:
                    #set quad two
                    ResourceMap = quadrantPlacement(2,10,10,18,1,Terrain,quadrants,ResourceMap)
                elif quad == 3 and quadrants[2] == False:
                    #set quad three
                    ResourceMap = quadrantPlacement(10,2,18,10,2,Terrain,quadrants,ResourceMap)
                elif quad == 4 and quadrants[3]==False:
                    # set quad four
                    ResourceMap = quadrantPlacement(10,18,18,18,3,Terrain,quadrants,ResourceMap)


    return ResourceMap
def quadrant(quadrant_numbers,row_num,col_num):
    minX = row_num/10
    minY = col_num/10
    maxX = row_num * 0.9
    maxY = row_num * 0.9
    quadSlice = math.sqrt(quadrant_numbers)
    xi = minX
    yi = minY
    for i in range(0,int(quadSlice)):
        xi += ((maxX - minX) / int(quadSlice))
        yi += ((maxY - minY) / int(quadSlice))

    print (xi)
    print (yi)


def quadrantPlacement(x1,y1,x2,y2,quadrant_num,Terrain,quadrants,ResourceMap):
    TerrainCount = 0
    for x in range(x1,x2):
        for y in range(y1,y2):
            for ix in range(x-1 , x + 1):
                for iy in range(y - 1, y + 2):
                    if 1 <= Terrain[ix][iy] <= 5:
                        TerrainCount += 1
                if TerrainCount == 6 and quadrants[quadrant_num] == False and 1 <=ResourceMap[x][y] <= 5:
                    ResourceMap[x][y] = 3
                    global setPos
                    setPos = False
                    quadrants[quadrant_num] = True
            TerrainCount = 0

    return ResourceMap

def Coastline(col_num,row_num,start_row,start_col,Matrix):
	w, h = 10, 10
	for y in range(int(start_col),int(col_num)):
		for x in range (int(start_row),int(row_num)):
			Matrix[x][y] = 0
	return Matrix
