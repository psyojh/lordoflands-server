import sys
from noise import pnoise2
import random
random.seed()
octaves = random.random()

freq = 16.0 * octaves
col_num = 50
row_num = 50

w, h = 10, 10
Matrix = [[0 for x in range(row_num)] for y in range(col_num)]
for y in range(col_num):
    for x in range(row_num):
        n = int(pnoise2(x/freq, y / freq, 1)*5+3)
        Matrix[x][y] = n


def Coastline(col_num,row_num,start_row,start_col):

    for y in range(start_col,col_num):
            for x in range (start_row,row_num):
                if Matrix[x][y] == 2:
                    Matrix[x][y] = 1
                elif Matrix[x][y] == 3:
                    #Random number for chance of water
                    water_chance = random.randint(0,9)
                    if water_chance > 5:
                            Matrix[x][y] = 1
                elif Matrix[x][y] > 4:
                        if(water_chance > 7):
                            Matrix[x][y] = 1






for y in range(col_num):
    for x in range(row_num):
        print Matrix[x][y],
    print "\n"
print "Original"

Coastline(3,50,0,0)
Coastline(50,3,0,0)
Coastline(3,50,47,0)
Coastline(50,3,0,47)


for y in range(col_num):
    for x in range(row_num):
        print Matrix[x][y],
    print "\n"





#print Matrix[0][0] # prints 1
#x, y = 0, 6
#print Matrix[x][y] # prints 3; be careful with indexing!
