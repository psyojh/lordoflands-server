from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from .models import *
from .serializers import *
from rest_framework.decorators import api_view
from .MapGenerations import *
from .Buildings import *
from .Map_Progress import *
from .Threading import *
from django.core import serializers
from rest_framework.response import Response
import sys
from datetime import *
from rest_framework.views import APIView
import json
from django.http import Http404
# REQUIRES DJANGO, DJANGO REST FRAMEWORK AND NOISE
#http://www.django-rest-framework.org/
#https://www.djangoproject.com/
#pip install noise
class User_Account_View(APIView):
	serializer_class = login_serializer

	def get (self, request):
		email = request.GET.get('email')
		password = request.GET.get('password')
		user_count = User_Account.objects.filter(Email = email, Password = password).count()
		if user_count == 1:
			user = User_Account.objects.get(Email = email)
			data = {
				'User_Acc_Id':int(user.User_Acc_Id),
				'User_Name':user.Username
				}

			return Response(data,status = status.HTTP_200_OK)
		else:
			return Response("Error: Invalid login", status=status.HTTP_400_BAD_REQUEST)

	def post(self, request):
		serializer = User_account_serializer(data = request.data)
		if serializer.is_valid():
			user_count = User_Account.objects.filter(Email = str(serializer.validated_data['Email'])).count()
			if int(user_count) > 0:
				return Response("Error: You already have an account", status=status.HTTP_400_BAD_REQUEST)
			else:
				serializer.save()
				return Response(serializer.data,status = status.HTTP_201_CREATED)
		else:
			return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
class Session_view(APIView):
	def get(self,request):
		if Session.objects.all().count() == 0:
			return Response("No sessions are available", status = status.HTTP_200_OK)
		else:
			sessions = Session.objects.filter(Is_full = False)
			serializer = Session_serializer(sessions,many = True)
			print (serializer.data)
			return Response(serializer.data,status = status.HTTP_200_OK)

	def post(self,request):
		serializer = Session_serializer(data = request.data)
		print(serializer.initial_data)
		number_of_players = int(serializer.initial_data['num_of_players'])
		session_name = serializer.initial_data['Session_name']
		sess_creator = serializer.initial_data['Session_creator']
		num_players = serializer.initial_data['num_of_players']
		timed_game = serializer.initial_data['timed_game']
		#playerNum =  int(playerNum)
		Terrain_map = []
		Resource_map = []
		Territory_map = []
		Unit_map = []
		map_size = MapSize(number_of_players)

		if map_size == 0:
			return Response("Error: Not enough players for game", status = status.HTTP_400_BAD_REQUEST)

		if int(Session.objects.filter(Session_name = session_name).count() )< 0:
			return Response("Error:Session already exists",status = HTTP_400_BAD_REQUEST)
		else:
			Terrain = TerrainGen(number_of_players,map_size,map_size)
			Resource = ResourceGen(Terrain,map_size,map_size,number_of_players)
			Territory = TerritoryGen(Resource,map_size,map_size)
			Units = UnitGen(Terrain,map_size,map_size,number_of_players)

			Terrain_map = MatrixToMap(Terrain,map_size,map_size)
			Resource_map = MatrixToMap(Resource, map_size,map_size)
			Territory_map = MatrixToMap(Territory, map_size,map_size)
			Unit_map = MatrixToMap(Units,map_size,map_size)
			session_save = Session(Session_name = session_name, Session_creator = User_Account.objects.get(User_Acc_Id = sess_creator), num_of_players = number_of_players,timed_game = timed_game)
			session_save.save()

			Session_number = session_save.Session_id
			MapSession = Map(Map_row = map_size, Map_col=map_size,TerrainMap=Terrain_map,ResourceMap = Resource_map,TerritoryMap = Territory_map,UnitMap= Unit_map,Session_id_id = Session_number)
			MapSession.save()

			session = Session.objects.get(Session_name = session_name)

			data = {
				'Session_id': int(session.Session_id),
				'Session_creator':int(session.Session_creator_id),
				'Session_name':str(session.Session_name),
				'num_of_players': int(session.num_of_players),
				'timed_game': int(session.timed_game)
				}

			return Response(data, status= status.HTTP_201_CREATED)
class Session_Players_view(APIView):
	def get(self, request):
		#Joining a pre-joined game
		session = request.GET.get('session')
		user_id_number = request.GET.get('user')
		session = float(session)
		user_id_number = float(user_id_number)

		Map_gen = Map.objects.get(Session_id = int(session)).TerrainMap
		Map_col= Map.objects.get(Session_id = int(session)).Map_col
		Map_row = Map.objects.get(Session_id = int(session)).Map_row
		Territory = Map.objects.get(Session_id = int(session)).TerritoryMap
		Resource =  Map.objects.get(Session_id = int(session)).ResourceMap
		Unit_map = Map.objects.get(Session_id = int(session)).UnitMap
		Current_Session_player_id = Session_Players.objects.get(User_Acc_id = user_id_number, Session_id = session)
		Map_gen = Map_gen[1:]
		Map_gen = Map_gen[:-8]
		curr_session = Session.objects.get(Session_id = session)
		start_time = 0

		if curr_session.Is_full:
			start_time = curr_session.start_time
			start_time = str(start_time)
			start_time = start_time.replace("-",".")
			start_time = start_time.replace(":",".")
			start_time = start_time.replace(" ","-")
			start_time = start_time[:19]

		data = {
			'Session_id':int(session),
			'map_row':Map_row,
			'map_col':Map_col,
			'map_gen':Map_gen,
			'Territory_map':Territory,
			'Resource_map':Resource,
			'Unit_map':Unit_map,
			'Session_player':Current_Session_player_id.Session_Players_id,
			'Time_start':start_time
		}
		#print (data)
		return Response(data,status= status.HTTP_200_OK)


	def post(self,request):
		#Joining a new game
		#Used for map generation and building generation
		serializer = Session_player_serializer(data = request.data)
		Map_row_obj = Map.objects.get(Session_id = serializer.initial_data['Session_id'])
		Map_row =  int(Map_row_obj.Map_row)
		Map_col= Map.objects.get(Session_id = serializer.initial_data['Session_id']).Map_col
		Map_gen = Map.objects.get(Session_id = serializer.initial_data['Session_id']).TerrainMap
		Territory = Map.objects.get(Session_id = serializer.initial_data['Session_id']).TerritoryMap
		Resource =  Map.objects.get(Session_id = serializer.initial_data['Session_id']).ResourceMap
		Unit = Map.objects.get(Session_id = serializer.initial_data['Session_id']).UnitMap
		user_id_number = int(serializer.initial_data['User_Acc_Id'])
		sesh_id_number = int(serializer.initial_data['Session_id'])
		Faction_name = serializer.initial_data['Faction']

		curr_session = Session.objects.get(Session_id = sesh_id_number)
		Session_player = Session_Players(User_Acc_id = User_Account.objects.get(User_Acc_Id = user_id_number),Session_id = Session.objects.get(Session_id = sesh_id_number),Faction = Faction_name)
		Session_player.save()
		Current_player = Session_Players.objects.get(User_Acc_id = user_id_number, Session_id = sesh_id_number)
		res = MapToMatrix(Resource,Map_col)
		terr = MapToMatrix(Territory,Map_col)
		terr_map = Player_territory(Current_player.Session_Players_id,terr,Map_col,res,curr_session)
		terr_list = listsToMap(terr_map,Map_col)

		Territory_map = Map.objects.get(Session_id = sesh_id_number)
		Territory_map.TerritoryMap = terr_list
		Territory_map.save()

		FinalTerritory = Map.objects.get(Session_id = sesh_id_number)

		Map_gen = Map_gen[1:]
		Map_gen = Map_gen[:-8]

		print(FinalTerritory.TerritoryMap)
		data = {
			'User_Acc_Id':serializer.initial_data['User_Acc_Id'],
			'Session_id':serializer.initial_data['Session_id'],
			'Faction':serializer.initial_data['Faction'],
			'map_row':Map_row,
			'map_col':Map_col,
			'map_gen':Map_gen,
			'Territory_map':FinalTerritory.TerritoryMap,
			'Resource_map':Resource,
			'Unit_map':Unit,
			'Session_player':Current_player.Session_Players_id
		}

		if Session_Players.objects.filter(Session_id = sesh_id_number).count() == 2:
		#curr_session.num_of_players:
			curr_session.Is_full = True
			curr_session.save()
			timerThread = TimeCheckingThread(1,"TimerThread1",1,int(serializer.initial_data['Session_id']))
			timerThread.start()

		return Response(data, status= status.HTTP_201_CREATED)
class Sessions_Joined_view(APIView):
	def get(self,request):
		player = request.GET.get('player')
		playerId = float(player)

		joined_sessions_arr = Session.objects.filter(session_players__User_Acc_id=int(playerId))
		serializer = Session_serializer(joined_sessions_arr,many = True)
		#joined_array = joined_sessions_arr.values_list('Session_id','Session_name','Session_creator','num_of_players','timed_game')
		print(serializer.data)
		return Response(serializer.data, status=status.HTTP_200_OK)
class Unit_Purchase_View(APIView):
	def post(self,request):
		#Used for purchasing units which cost food and gold
			serializer = Unit_purchase_serializer(data = request.data)

			Unit_Type = serializer.initial_data['Unit_Type']
			Unit_Size = serializer.initial_data['Unit_Size']
			Unit_Owner = serializer.initial_data['Unit_Owner']
			Session_id = serializer.initial_data['Session_Id']
			Unit_X = serializer.initial_data['Unit_X']
			Unit_Y = serializer.initial_data['Unit_Y']

			name = "Unit"
			attack = 0
			food_cost = 0
			if Unit_Type== 1:
				name = "Infantry"
				attack = 5
				food_cost = 2
			elif Unit_Type == 2:
				name = "Cavalry"
				attack = 10
				food_cost = 5
			elif Unit_Type == 3:
				name = "Special"
				attack = 12
				food_cost = 7

			session = Session.objects.get(Session_id = Session_id)
			player = Session_Players.objects.get(Session_Players_id = float(Unit_Owner))
			if player.Food >= food_cost:
				unit_bought = Units(Unit_Type = Unit_Type, Unit_Name = name, Unit_Size = int(Unit_Size),Unit_Attack = attack, Unit_Owner = player,Unit_X = int(Unit_X),Unit_Y=int(Unit_Y),Session_Id=session)
				unit_bought.save()
				unit_id = unit_bought.Units_id
				print(unit_id)
				Unit = Map.objects.get(Session_id = Session_id)
				player.Food = player.Food - food_cost
				player.save()
				UnitMatrix = MapToMatrix(Unit.UnitMap,Unit.Map_col)
				TerritoryMatrix = MapToMatrix(Unit.TerritoryMap,Unit.Map_col)
				units = Unit_placement(unit_id,UnitMatrix,int(Unit_X),int(Unit_Y))
				territory = Unit_Territory_placement(float(Unit_Owner),TerritoryMatrix,int(Unit_X),int(Unit_Y))
				territory = listsToMap(territory,int(Unit.Map_col))
				units = listsToMap(units,int(Unit.Map_col))
				Unit.UnitMap = units
				Unit.TerritoryMap = territory
				Unit.save()

				data = {
					'Session_Id':serializer.initial_data['Session_Id'],
					'Unit_Type':serializer.initial_data['Unit_Type'],
					'Unit_Size':serializer.initial_data['Unit_Size'],
					'Unit_Owner':serializer.initial_data['Unit_Owner'],
					'Unit_X':serializer.initial_data['Unit_X'],
					'Unit_Y':serializer.initial_data['Unit_Y'],
					'Unit_id':unit_id,
				}
				print(data)

				return Response(data, status=status.HTTP_200_OK)

			else:return Response("Error: Insufficient food",status=status.HTTP_200_OK)
class Unit_Movement_View(APIView):
	def get(self,request):
		#MOvement patternes which start with the threading
		session =request.GET.get('session')
		exceptUnits = request.GET.get('units')
		if exceptUnits == 0:
			exceptUnits = [0]
		else:
			exceptUnits = exceptUnits.split(',')
			exceptUnits = exceptUnits[-1]
		units = Units
		unit_moves = Unit_Movement
		unitIds = []
		unitNames = []
		unitAtt = []
		unitSize = []
		unitOwners = []
		unitX = []
		unitY = []
		unit_endTime = []
		try:
			units = Units.objects.filter(Session_Id = float(session)).exclude(Session_Id__in=exceptUnits)
		except units.DoesNotExist:
			raise Http404("No MyModel matches the given query.")

		for i in units:
			unitIds.append(i.Units_id)
			unitOwners.append(i.Unit_Owner_id)
			unitNames.append(i.Unit_Name)
			unitAtt.append(i.Unit_Attack)
			unitSize.append(i.Unit_Size)

			print (unit_moves)
			try:
				unit_moves = Unit_Movement.objects.filter(unit_id = i.Units_id)
				for j in unit_moves:
					print (j.Unit_X)
					unitX.append(j.Unit_X)
					unitY.append(j.Unit_Y)
					end_time = j.end_time + timedelta(hours = 1)
					end_time = str(end_time)
					end_time = end_time.replace("-",".")
					end_time = end_time.replace(":",".")
					end_time = end_time.replace(" ","-")
					end_time = end_time[:19]
					unit_endTime.append(end_time)
				unitX.append('#')
				unitY.append('#')
				unit_endTime.append('#')
			except unit_moves.DoesNotExist:
				raise Http404("No MyModel matches the given query.")
	#			unit = Units.objects.get(Units_id = i.Units_id)

		data= {
			'UnitsIds': ','.join(str(e) for e in unitIds),
			'UnitNames':','.join(str(e) for e in unitNames),
			'UnitAttack':','.join(str(e) for e in unitAtt),
			'UnitSize':','.join(str(e) for e in unitSize),
			'UnitOwners':','.join(str(e) for e in unitOwners),
			'UnitX':','.join(str(e) for e in unitX),
			'UnitY':','.join(str(e) for e in unitY),
			'UnitEndTimes':','.join(str(e) for e in unit_endTime)
			}
		print(data)


		return Response(data, status = status.HTTP_200_OK)
	def post(self, request):
		serializer = Unit_movement_serializer(data = request.data)
		unit = serializer.initial_data['unit_id']
		print (unit)
		x_pos = serializer.initial_data['Unit_X']
		y_pos = serializer.initial_data['Unit_Y']
		end_time = serializer.initial_data['end_times']
		print (end_time)
		session = serializer.initial_data['Session_id']
		x_pos = [x.strip() for x in x_pos.split('#')]
		y_pos = [y.strip() for y in y_pos.split('#')]
		end_time = [t.strip() for t in end_time.split('#')]
		i = 0
		print("HUYIHIYB")
		print(unit)
		moving_unit = Units.objects.get(Units_id = int(unit))
		curr_session = Session.objects.get(Session_id = int(session))

		for i in range(0,len(x_pos)):
			if x_pos[i] == '':
				continue
			else:
				print(end_time[i])
				end_time[i] = datetime.strptime(end_time[i], '%d %b %Y %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')

				move = Unit_Movement(unit_id = moving_unit,Unit_X = int(x_pos[i]),Unit_Y = int(y_pos[i]),end_time = end_time[i],Session_id = curr_session)
				move.save()
		print("Unit")
		print(unit)

		print("x_poss")
		print(x_pos)

		print("y_poss")
		print(y_pos)

		print("end")
		print(end_time)

		print (serializer.initial_data)
		return Response(serializer.initial_data,status = status.HTTP_200_OK)
class Unit_Attack_View(APIView):
	def get(self,request):
		sessions = Session.objects.all()
class Buildings_Purchase_view(APIView):
	def post(self,request):

		build_serializer = Building_purchase_serializer(data = request.data)
		print (build_serializer.initial_data)
		Type = build_serializer.initial_data['Building_Type']
		Owner = build_serializer.initial_data['Building_Owner']
		X = build_serializer.initial_data['Building_X']
		Y = build_serializer.initial_data['Building_Y']
		Session_id = build_serializer.initial_data['Session_Id']
		Owner = float(Owner)
		name = "BUILDING"
		attack = 0
		defense = 0
		gold_cost = 0
		build_cost = 0
		description = "NONE"

		if int(Type) == 2:
			name = 'Fort'
			attack = 20
			defense = 30
			description = "This fortress is used to build units and keep you safe!"
			gold_cost = 20
			build_cost = 15

		elif int(Type) == 3:
			name = 'Townhall'
			attack = 40
			defense = 50
			description = "Your central base for all activities and to please your populus"
			gold_cost = 40
			build_cost = 30

		elif int(Type) == 4:
			name = 'Farm'
			attack = 10
			defense = 15
			description = "Where you grow all your food for your populus"
			gold_cost = 15
			build_cost = 10

		elif int(Type) == 5:
			name = 'Gold_Mine'
			attack = 15
			defense = 20
			description = "Where you mine all the gold for your economy"
			gold_cost = 30
			build_cost = 20

		elif int(Type) == 6:
			name = 'Stone_Mine'
			attack = 15
			defence = 20
			description = "Where you mine buildings materials"
			gold_cost = 20
			build_cost = 20

		session = Session.objects.get(Session_id = Session_id)
		player = Session_Players.objects.get(Session_Players_id = int(Owner))
		if int(player.Gold) > gold_cost and int(player.Building_resources) > build_cost:
			building = Buildings(Building_Type = Type,Session_Id=session,Building_name=name,Buidling_description=description,Building_Attack=attack,Building_defense=defense,Building_Owner = player,Building_X=X,Building_Y=Y)
			building.save()
			print (int(player.Gold) - gold_cost)
			total_gold = int(player.Gold) - gold_cost
			player.Gold = total_gold
			print(int(player.Building_resources) - build_cost)
			total_build = int(player.Building_resources) - build_cost
			player.Building_resources = total_build

			player.save()

			Building_map = Map.objects.get(Session_id = Session_id)
			BuildingMatrix = MapToMatrix(Building_map.ResourceMap,int(Building_map.Map_col))
			TerritoryMatrix = MapToMatrix(Building_map.TerritoryMap,int(Building_map.Map_col))

			build = Building_placement(Type,BuildingMatrix,int(X),int(Y),int(Building_map.Map_col))
			territory = Building_territory(int(Owner),TerritoryMatrix,int(X),int(Y),int(Building_map.Map_col))

			builds = listsToMap(build,int(Building_map.Map_col))
			territorys = listsToMap(territory,int(Building_map.Map_col))
			Building_map.ResourceMap = builds
			Building_map.TerritoryMap = territorys
			Building_map.save()

			return Response(build_serializer.initial_data, status=status.HTTP_200_OK)
		else:
			return Response("Error:Insufficient resources",status=status.HTTP_400_BAD_REQUEST)

class Message_view(APIView):
	def get(self,request):
		Session = request.GET.get('Session_id')
		Message_Arr = Messages.objects.filter(Session_id = Session)
		if Message_Arr.count() == 0:
			return Response("Error:No messages available", status = status.HTTP_400_BAD_REQUEST)
		else:
			serializer = Message_serializer(Message_Arr,many = True)
			return Response(serializer.data,status = status.HTTP_200_OK)

	def post(self,request):
		serializer = Message_serializer(data = request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data,status = status.HTTP_200_OK)
		else:
			return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
class Session_Player_Group_view(APIView):
	def get(self,request):
		session_param = request.GET.get('Session_id')
		session_id = float(session_param)
		curr_session = Session.objects.get(Session_id = int(session_id))
		Session_player_ids = Session_Players.objects.filter(Session_id = session_id).values_list('Session_Players_id',flat = True)
		Factions = Session_Players.objects.filter(Session_id = session_id).values_list('Faction',flat = True)
		User_account_ids = Session_Players.objects.filter(Session_id = session_id).values_list('User_Acc_id',flat = True)
		User_names = []
		for i in User_account_ids:
			User_names.append(User_Account.objects.get(User_Acc_Id = i).Username)
		Session_player_ids  = ','.join(str(e) for e in Session_player_ids)
		Factions = ','.join(str(e) for e in Factions)
		User_names = ','.join(str(e) for e in User_names)

		data = {
		'Session_player_id': Session_player_ids,
		'User_name': User_names,
		'Faction':Factions,
		}

		print(data)
		return Response(data, status=status.HTTP_200_OK)
class User_Economy_view(APIView):
	def get(self,request):
		player = request.GET.get('Session_player')
		Session_player = Session_Players.objects.get(Session_Players_id = float(player))
		data = {
			'Gold':Session_player.Gold,
			'Building_resources':Session_player.Building_resources,
			'Food':Session_player.Food
		}
		return Response(data,status = status.HTTP_200_OK)
def MatrixToMap(Matrix,col_num,row_num):
	mapping = []
	for y in range(col_num):
		for x in range(row_num):
			mapping.append(Matrix[x][y])
		mapping.append('r')
	return mapping

def listsToMap(lists,MapSize):
	mapping = []
	for y in range(0,MapSize):
		for x in range(0,MapSize):
			lists[x][y] = lists[x][y].replace("\'","")
			if lists[x][y] == ' \'0\']':
				mapping.append('0')
			if lists[x][y] == ' [\'0\'':
				mapping.append('0')
			else:
				mapping.append(lists[y][x])
		mapping.append('r')

	return mapping

def MapToMatrix(Map,MapSize):
	list_of_rows = []
	split_list = []
	row = []
	i = "  "
	Map = Map.replace("[", "")
	Map = Map.replace("]", "")
	Map = Map.replace(" ", "")
	Map = Map.replace("\'","")

	split_list = Map.split(',')

	for i in split_list:
		if i == "r":
			list_of_rows.append(list(row))
			row = []
		elif i == "," or i == " "or i== "[" or i == "]" or i == "\""or i == "\'" :
			continue
		else:
			row.append(i)
	return list_of_rows

def MapSize(number_of_players):
	if number_of_players < 3:
		return 0
	elif number_of_players == 3:
		return 20
	elif 4 <= number_of_players <=6:
		return 30
	elif 7 <= number_of_players <= 10:
		return 50
