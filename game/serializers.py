from rest_framework import serializers
from .models import *

class User_account_serializer(serializers.ModelSerializer):
	class Meta:
		model = User_Account
		fields = ('Full_name','Email','Username','Password','City','Date_of_birth')

class Session_serializer(serializers.ModelSerializer):
	class Meta:
		model = Session
		fields = ('Session_id','Session_name','Session_creator','num_of_players','timed_game')

class Session_player_serializer(serializers.ModelSerializer):
	class Meta:
		model = Session_Players
		fields = ('User_Acc_id','Session_id','Faction')

class Unit_purchase_serializer(serializers.ModelSerializer):
	class Meta:
		model = Units
		fields = ('Session_Id','Unit_Type','Unit_Size','Unit_Owner', 'Unit_X','Unit_Y')

class Building_purchase_serializer(serializers.ModelSerializer):
	class Meta:
		model = Buildings
		fields = ('Session_Id','Building_Type','Building_Owner','Building_X','Building_Y')

class login_serializer(serializers.Serializer):
	class Meta:
		model = User_Account

class Message_serializer(serializers.ModelSerializer):
	class Meta:
		model = Messages
		fields = ('Sender_id','Message','Session_id')

class Unit_movement_serializer(serializers.ModelSerializer):
	class Meta:
		model = Unit_Movement
		fields = ('unit_id','Unit_X','Unit_Y','start_time','end_times','Session_id')
