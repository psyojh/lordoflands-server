from __future__ import unicode_literals
from django.utils import timezone
from django.db import models

class User_Account(models.Model):
	User_Acc_Id = models.AutoField(primary_key=True)
	Full_name = models.CharField(max_length=200)
	Email = models.CharField(max_length = 200)
	Username = models.CharField(max_length = 200)
	Password = models.CharField(max_length = 30)
	City = models.CharField(max_length = 40)
	Date_of_birth = models.DateField()
	Games_Won = models.IntegerField(default = 0)
	Games_Lost =  models.IntegerField(default = 0)
	Xp = models.IntegerField(default = 0)
	Games_Played = models.IntegerField(default = 0)
	Date_created= models.DateTimeField(auto_now_add=True)

class Session(models.Model):
	Session_id = models.AutoField(primary_key=True)
	Session_creator = models.ForeignKey(User_Account,on_delete= models.CASCADE)
	Session_name = models.CharField(max_length=200)
	Is_full = models.BooleanField(default = False)
	num_of_players = models.IntegerField(default = 0)
	timed_game = models.IntegerField(default = 0)
	start_time = models.DateTimeField(null = True)
	Created_on = models.DateTimeField(auto_now_add=True)
	Is_playing = models.BooleanField(default = False)
	Is_finished = models.BooleanField(default = False)

class Map(models.Model):
	Map_id = models.AutoField(primary_key=True)
	Map_row = models.IntegerField(default=0)
	Map_col = models.IntegerField(default=0)
	TerrainMap = models.CharField(max_length=2000,default="None")
	ResourceMap = models.CharField(max_length = 2000,default="None")
	TerritoryMap = models.CharField(max_length = 2000, default="None")
	UnitMap = models.CharField(max_length = 2000, default="None")
	Session_id = models.ForeignKey(Session, on_delete=models.CASCADE)

class Session_Players(models.Model):
	Session_Players_id = models.AutoField(primary_key=True)
	User_Acc_id = models.ForeignKey(User_Account, on_delete = models.CASCADE)
	Session_id = models.ForeignKey(Session, on_delete=models.CASCADE)
	Faction = models.CharField(default="p",max_length=50)
	Gold = models.IntegerField(default = 100)
	Building_resources = models.IntegerField(default = 50)
	Food =  models.IntegerField(default = 30)


class Buildings(models.Model):
	Building_id = models.AutoField(primary_key = True)
	Building_Type = models.IntegerField(default = 0)
	Session_Id = models.ForeignKey(Session,on_delete = models.CASCADE,default=0)
	Building_name = models.CharField(max_length = 100)
	Buidling_description = models.CharField(max_length = 200)
	Building_Attack = models.IntegerField(default = 0)
	Building_defense = models.IntegerField(default = 0)
	Building_Owner = models.ForeignKey(Session_Players, on_delete = models.CASCADE)
	Building_X = models.IntegerField(default = 0)
	Building_Y = models.IntegerField(default = 0)
	Building_gold_cost = models.IntegerField(default = 0)
	Building_resource_cost = models.IntegerField(default = 0)

class Units(models.Model):
	Units_id = models.AutoField(primary_key = True)
	Session_Id = models.ForeignKey(Session, on_delete = models.CASCADE,default = 0)
	Unit_Type = models.IntegerField(default = 0)
	Unit_Name = models.CharField(max_length = 100,default="UNIT")
	Unit_Size = models.IntegerField(default = 0)
	Unit_Attack = models.IntegerField(default = 0)
	Unit_Owner = models.ForeignKey(Session_Players, on_delete = models.CASCADE,default = 0)
	Unit_X = models.IntegerField(default = 0)
	Unit_Y = models.IntegerField(default = 0)
	Unit_food_cost = models.IntegerField(default = 0)

class Tile(models.Model):
	Tile_Id = models.AutoField(primary_key = True)
	Is_occupied = models.BooleanField(default = False)
	Occupied_by = models.ForeignKey(User_Account, on_delete = models.CASCADE)
	Unit_id = models.ForeignKey(Units,on_delete = models.CASCADE)
	Is_building = models.BooleanField(default = False)
	Building_id = models.ForeignKey(Buildings, on_delete = models.CASCADE)

class Unit_Attacks(models.Model):
	host_unit_id = models.ForeignKey(Units, on_delete = models.CASCADE,related_name="Host")
	opp_unit_id = models.ForeignKey(Units, on_delete = models.CASCADE,related_name="Opponent")
	Session_id = models.ForeignKey(Session, on_delete = models.CASCADE)
	time_of_battle = models.DateTimeField(auto_now_add=True)
	result_of_battle = models.CharField(default = "Pending",max_length= 100)

class Building_Attacks(models.Model):
	unit = models.ForeignKey(Units, on_delete = models.CASCADE)
	building = models.ForeignKey(Buildings, on_delete = models.CASCADE)
	Session_id = models.ForeignKey(Session, on_delete = models.CASCADE)
	time_of_battle = models.DateTimeField(auto_now_add = True)
	result_of_battle = models.CharField(default = 'Pending',max_length = 100)

class Unit_Movement(models.Model):
	unit_id = models.ForeignKey(Units,on_delete= models.CASCADE)
	Unit_X =  models.IntegerField(default = 0)
	Unit_Y = models.IntegerField(default = 0)
	end_time = models.DateTimeField(null = True)
	Session_id = models.ForeignKey(Session,on_delete= models.CASCADE)

class Messages(models.Model):
	Message_id = models.AutoField(primary_key = True)
	Sender_id = models.ForeignKey(User_Account,on_delete = models.CASCADE,related_name = "Sender")
	Message = models.CharField(default = "Message",max_length = 135)
	Session_id = models.ForeignKey(Session,on_delete = models.CASCADE)
	Time_of_message = models.DateTimeField(auto_now_add=True)
