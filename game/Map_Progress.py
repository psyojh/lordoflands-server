
from .models import *
#checks for a number between 0 and 11 and replcaes it with the player id number
def Player_territory(player_number,mapper,map_size,build_map,session):
    Mapping = Map.objects.get(Session_id = session)
    primary = 0
    for i in range (0,map_size):
        for j in range(0,map_size):
            if mapper[i][j] == "'":
                continue
            elif primary == 0:
                if 0 < int(mapper[i][j]) < 11:
                    primary = int(mapper[i][j])
                    mapper[i][j] = str(player_number)
            else:
                if isinstance(mapper[i][j], float):
                    continue
                elif int(mapper[i][j]) == primary:
                    mapper[i][j] = str(player_number)
                if int(build_map[i][j]) > 1:
                    if Buildings.objects.filter(Building_X = i,Building_Y = j,Session_Id = session).count() == 1:
                        continue
                    else:
                        build = Buildings(Building_Owner = Session_Players.objects.get(Session_Players_id = player_number), Building_Type = 3,Session_Id = session,Building_name="Townhall",Buidling_description="Your central base for all activities and to please your populus",Building_Attack = 40,Building_defense= 50,Building_X = i,Building_Y=j)
                        build.save()

    return mapper

#Unit building and territory are all the same placement style methods

def Unit_placement(unit,unit_mapper,Unit_X,Unit_Y):
    unit_mapper[Unit_Y][Unit_X] = str(unit)
    return unit_mapper

def Unit_Territory_placement(player_number,territory_map,Terr_X,Terr_Y):
    territory_map[Terr_Y][Terr_X] = str(player_number)
    return territory_map

def Building_placement(building_type,build_map,building_x, building_y, map_size):
    build_map[building_y][building_x] = str(building_type)
    return build_map

#has a 2 - 3 - 2 style territory
def Building_territory(player,territory_map,build_x,build_y,map_size):
    print (territory_map)
    if build_y % 2 == 1:
        for m in range (build_x - 1, build_x + 1):
            for n in range(build_y-1 , build_y + 2):
                territory_map[n][m] = str(player)
                territory_map[build_y][build_x+1] = str(player)
    else:
        for m in range (build_x , build_x + 2):
            for n in range(build_y -1, build_y + 2):
                territory_map[n][m] = str(player)
                territory_map[build_y][build_x-1] = str(player)

    return territory_map
