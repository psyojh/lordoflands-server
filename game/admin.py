from django.contrib import admin
from .models import *
admin.site.register(User_Account)
admin.site.register(Session)
admin.site.register(Session_Players)
admin.site.register(Map)
admin.site.register(Messages)
admin.site.register(Unit_Movement)
admin.site.register(Unit_Attacks)
admin.site.register(Buildings)
