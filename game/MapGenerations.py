from noise import pnoise2
import random
from .Buildings import *

def Coastline(col_num,row_num,start_row,start_col,Matrix):
	w, h = 10, 10
	for y in range(int(start_col),int(col_num)):
		for x in range (int(start_row),int(row_num)):
			Matrix[x][y] = 0
	return Matrix

def TerrainGen(num_of_players,col_num,row_num):
	random.seed()
	octaves = random.random()
	freq = 16.0 * octaves
	w, h = 10, 10
	Terrain = [[0 for x in range(row_num)] for y in range(col_num)]

	for y in range(col_num):
		for x in range(row_num):
			n = int(pnoise2(x/freq, y / freq, 1)*5+3)
			Terrain[x][y] = n

	Coastline(col_num,row_num,row_num*0.9,0,Terrain)
	Coastline(col_num/10,row_num,0,0,Terrain)
	Coastline(col_num,row_num/10,0,0,Terrain)
	Coastline(col_num,row_num,0,col_num*0.9,Terrain)

	return Terrain

def TerritoryGen(Resource,col_num,row_num):
	player = 1
	TerritoryMap = [[0 for x in range(row_num)]for y in range(col_num)]

# Sets the territory to the default blank to start
	for x in range(row_num):
		for y in range(col_num):
			TerritoryMap[x][y] = 0

#Creates a border for territory to avoid unplayable areas
	Coastline(col_num/10,row_num,0,0,TerritoryMap)
	Coastline(col_num,row_num/10,0,0,TerritoryMap)
	Coastline(col_num,row_num/10,row_num*0.9,0,TerritoryMap)
	Coastline(col_num,row_num,0,col_num*0.9,TerritoryMap)

#Used to create the circular area of territory around the townhall
	for y in range(col_num):
		for x in range(row_num):
			if Resource[x][y] == 3:
				if y % 2 == 1:
					for m in range (y - 1, y + 2):
						for n in range(x-1 , x + 1):
							TerritoryMap[n][m] = player
							TerritoryMap[x+1][y] = player
					player += 1
				else:
					for m in range (y - 1, y + 2):
						for n in range(x , x + 2):
							TerritoryMap[n][m] = player
							TerritoryMap[x-1][y] = player
					player += 1
	return TerritoryMap

def ResourceGen(Matrix, col_num, row_num,number_players):
    ResourceMap = [[0 for x in range(row_num)] for y in range(col_num)]
    for x in range(row_num):
        for y in range(col_num):
            ResourceMap[x][y] = 1

    Coastline(col_num/10,row_num,0,0,ResourceMap)
    Coastline(col_num,row_num/10,0,0,ResourceMap)
    Coastline(col_num,row_num/10,row_num*0.9,0,ResourceMap)
    Coastline(col_num,row_num,0,col_num*0.9,ResourceMap)
    ResourceSpawn = Townhall_gen(Matrix,ResourceMap,col_num,row_num,number_players)

    return ResourceSpawn

def UnitGen(Terrain,col_num,row_num, num_of_players):
	UnitMap = [[0 for x in range(row_num)]for y in range(col_num)]
	for x in range(row_num):
		for y in range(col_num):
			UnitMap[x][y] = 0
	return UnitMap
