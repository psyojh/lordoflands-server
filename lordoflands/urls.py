"""lordoflands URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import include,url
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns
from game import views


urlpatterns = [
#Covers all Session and account creation
    url(r'^admin/', admin.site.urls),
    url(r'^user/$', views.User_Account_View.as_view()),
    url(r'^session/$', views.Session_view.as_view()),
    url(r'^session-player/$', views.Session_Players_view.as_view()),
    url(r'^sessions-joined/$',views.Sessions_Joined_view.as_view()),
    url(r'^user_economy/$',views.User_Economy_view.as_view()),
#Covers all game mechanics
    url(r'^unit_movement/$', views.Unit_Movement_View.as_view()),
    url(r'^unit_attack/$',views.Unit_Attack_View.as_view()),
#Covers purchasing Units and Buildings
    url(r'^unit_buy/$',views.Unit_Purchase_View.as_view()),
    url(r'^building_buy/$',views.Buildings_Purchase_view.as_view()),
    url(r'^session_chat/$',views.Message_view.as_view()),
    url(r'^session-player-group/$',views.Session_Player_Group_view.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
